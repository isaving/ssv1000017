//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000017/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000017Controller struct {
	controllers.CommTCCController
}

func (*Ssv1000017Controller) ControllerName() string {
	return "Ssv1000017Controller"
}

// @Desc ssv1000017 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000017Controller) Ssv1000017() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000016Controller.Ssv1000016 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000017I := &models.SSV1000017I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000017I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000017I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000017 := &services.Ssv1000017Impl{}
	ssv1000017.New(c.CommTCCController)
	ssv1000017.Ssv100017I = ssv1000017I
	ssv1000017Compensable := services.Ssv1000017Compensable

	proxy, err := aspect.NewDTSProxy(ssv1000017, ssv1000017Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000017I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000017O, ok := rsp.(*models.SSV1000017O); ok {
		if responseBody, err := models.PackResponse(ssv1000017O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv1000017 Controller
// @Description ssv1000017 controller
// @Param Ssv1000017 body model.SSV1000017I true body for SSV1000017 content
// @Success 200 {object} model.SSV1000017O
// @router /create [post]
func (c *Ssv1000017Controller) SWSsv1000017() {
	//Here is to generate API documentation, no need to implement methods
}
