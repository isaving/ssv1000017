//Version: v0.01
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SAC0000003I struct {
	CustId     string `json:"CustId" description:"Customer number"`        //客户号
	ContractId string `json:"ContractId" description:"Contract number"`    //合约号
	Account    string `json:"Account" description:"Account number"`        //核算账号
	Amount     float64 `json:"Amount" description:"Transaction amount"`     //交易金额
	TrnDate    string `json:"TrnDate" description:"Transaction date"`      //业务日期
	TrnSeq     string `json:"TrnSeq" description:"Business serial number"` //业务流水
	FlowSeq    int     `json:"FlowSeq"`
}

type SAC0000003O struct {
	AmtFreezing  float64 `json:"AmtFreezing"`  //冻结金额
	AmtCurrent   float64 `json:"AmtCurrent"`   //当前余额
	AmtLast      float64 `json:"AmtLast"`      //上期余额
	AmtAvailable float64 `json:"AmtAvailable"` //可用余额   可用余额=当前余额-冻结金额-预留金额
	AccStatus    string  `json:"AccStatus"`    //账户状态  1-有效  0-无效

}

// @Desc Build request message
func (o *SAC0000003I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SAC0000003I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SAC0000003O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SAC0000003O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SAC0000003I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SAC0000003I) GetServiceKey() string {
	return "SAC0000003"
}
