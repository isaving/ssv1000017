//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var AC000003 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "AmtFreezing":1,
		"AmtCurrent":1,
		"AmtLast":1,
		"AmtAvaliable":1,
		"AccStatus":"1"
    }
}`

func (this *Ssv1000017Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "AC000003":
		responseData = []byte(AC000003)

	}

	return responseData, nil
}

func TestSsv1000017Impl_Ssv1000017(t *testing.T) {

	Ssv10000171Impl := new(Ssv1000017Impl)

	_, _ = Ssv10000171Impl.TrySsv1000017(&models.SSV1000017I{
		CustId:     "1",
		ContractId: "1",
		Account:    "1",
		Amount:     1,
		TrnDate:    "1",
		TrnSeq:     "1",
		FlowSeq:    0,
	})

	_, _ = Ssv10000171Impl.ConfirmSsv1000017(&models.SSV1000017I{})
	_, _ = Ssv10000171Impl.CancelSsv1000017(&models.SSV1000017I{})

}
