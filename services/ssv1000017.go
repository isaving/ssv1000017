//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000017/constant"
	constant2 "git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)

var Ssv1000017Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000017",
	ConfirmMethod: "ConfirmSsv1000017",
	CancelMethod:  "CancelSsv1000017",
}

type Ssv1000017 interface {
	TrySsv1000017(*models.SSV1000017I) (*models.SSV1000017O, error)
	ConfirmSsv1000017(*models.SSV1000017I) (*models.SSV1000017O, error)
	CancelSsv1000017(*models.SSV1000017I) (*models.SSV1000017O, error)
}

type Ssv1000017Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv100017O  *models.SSV1000017O
	Ssv100017I  *models.SSV1000017I
	Ac000003O   *models.SAC0000003O
	srcBizSeqNo string
}

// @Desc Ssv1000017 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000017Impl) TrySsv1000017(ssv1000017I *models.SSV1000017I) (ssv1000017O *models.SSV1000017O, err error) {

	impl.Ssv100017I = ssv1000017I
	impl.srcBizSeqNo = impl.SrcAppProps[constant2.SRCBIZSEQNO]

	//调用存款存入BSS
	err = impl.AccountTransferIn()
	if err != nil {
		return nil, err
	}

	ssv1000017O = &models.SSV1000017O{
		AmtFreezing:  impl.Ac000003O.AmtFreezing,  //冻结金额
		AmtCurrent:   impl.Ac000003O.AmtCurrent,   //当前余额
		AmtLast:      impl.Ac000003O.AmtLast,      //上期余额
		AmtAvaliable: impl.Ac000003O.AmtAvailable, //可用余额
		AccStatus:    impl.Ac000003O.AccStatus,    //账户状态
	}

	return ssv1000017O, nil
}

//存款转入BSS
func (impl *Ssv1000017Impl) AccountTransferIn() error {
	sac0000003I := models.SAC0000003I{
		CustId:     impl.Ssv100017I.CustId,     //客户号
		ContractId: impl.Ssv100017I.ContractId, //合约号
		Account:    impl.Ssv100017I.Account,    //核算账号
		Amount:     impl.Ssv100017I.Amount,     //交易金额
		TrnDate:    impl.Ssv100017I.TrnDate,    //交易日期
		TrnSeq:     impl.srcBizSeqNo,           //交易流水号
		FlowSeq:    1,
	}

	//pack request
	reqBody, err := sac0000003I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.AC000003, reqBody)
	if err != nil {
		return err
	}

	ac000003O := models.SAC0000003O{}
	err = ac000003O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	impl.Ac000003O = &ac000003O

	return nil
}

func (impl *Ssv1000017Impl) ConfirmSsv1000017(ssv1000017I *models.SSV1000017I) (ssv1000017O *models.SSV1000017O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv1000017")
	return nil, nil
}

func (impl *Ssv1000017Impl) CancelSsv1000017(ssv1000017I *models.SSV1000017I) (ssv1000017O *models.SSV1000017O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv1000017")
	return nil, nil
}
