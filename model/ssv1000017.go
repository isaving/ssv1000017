//Version: v0.0.1
package model

type SSV1000017I struct {
	CustId     string `json:"CustId" description:"Customer number"`        //客户号
	ContractId string `json:"ContractId" description:"Contract number"`    //合约号
	Account    string `json:"Account" description:"Account number"`        //核算账号
	Amount     string `json:"Amount" description:"Transaction amount"`     //交易金额
	TrnDate    string `json:"TrnDate" description:"Transaction date"`      //业务日期
	TrnSeq     string `json:"TrnSeq" description:"Business serial number"` //业务流水
}

type SSV1000017O struct {
	AmtFreezing  float64 `json:"AmtFreezing" description:"Frozen amount"`      //冻结金额
	AmtCurrent   float64 `json:"AmtCurrent" description:"Current balance"`     //当前余额
	AmtLast      float64 `json:"AmtLast" description:"Previous balance"`       //上期余额
	AmtAvaliable float64 `json:"AmtAvaliable" description:"Available balance"` //可用余额   可用余额=当前余额-冻结金额-预留金额
	AccStatus    string  `json:"AccStatus" description:"Account status"`       //账户状态  1-有效  0-无效
}
